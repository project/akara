/**
 * @file
 * Global utilities.
 *
 */
(function (Drupal) {

  'use strict';

  Drupal.behaviors.akara = {
    attach: function (context, settings) {
      jQuery(window).scroll(function() {
        if (jQuery(document).scrollTop() > 50) {
          jQuery(".home-menu-logo").hide(1000);
          jQuery("#navbar-main").height('2.2rem');
          jQuery(".home-menu-logo img").attr('src', '/themes/contrib/akara/images/logo/iskcon_title.png');
          jQuery(".home-menu-logo img").height('auto');
        } else {
          jQuery("#navbar-main").height('auto');
          jQuery(".path-frontpage #main-wrapper").css
          jQuery(".home-menu-logo img").attr('src', '/themes/contrib/akara/images/logo/logo.png');
          jQuery(".home-menu-logo img").height('4.7rem');
        }
      });
    }
  };

})(Drupal);
