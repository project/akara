## Ākāra - A Bootstrap 5 theme

Ākāra is a theme based on
Bootstrap 5, a sleek, intuitive, and powerful front-end framework
for faster and easier web development.
Ākāra focus on layout flexibility
and customization using theme parameters.

- Layout is handle by Bootstrap.
- Two columns, with configurable widths.
- Fixed and fluid layouts.
- Configurable header behavior, using Bootstrap "fixed to top".
- Any block could be defined as "modal" with one click using Skinr.
- Full screen width slider region.

## Dependencies

- Install [Bootstrap Library module](https://www.drupal.org/project/bootstrap_library) to load the libraries.
- Install [Barrio Bootstrap 5 Theme](https://www.drupal.org/project/bootstrap_barrio) as the base theme.

## Installation

Unpack and upload Ākāra Theme on /themes folder.
